#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from pathlib import Path
import seaborn as sns

sns.set(style="white", color_codes=True)


def half_sample_mode(data: np.ndarray, sorted: bool = False):
    if not sorted:
        data = np.sort(data)

    if len(data) == 1:
        return data
    if len(data) == 2:
        return np.mean(data)
    if len(data) == 3:
        if data[1] - data[0] < data[2] - data[1]:
            return np.mean(data[0:1])
        elif data[1] - data[0] > data[2] - data[1]:
            return np.mean(data[1:2])
        elif data[1] - data[0] == data[2] - data[1]:
            return data[1]

    half_idx = int((len(data) + 1) / 2)  # Redondea para incluir el valor intermedio, en el caso de una cantidad impar
    # de elementos

    # Calculate all interesting ranges that span half of all data points
    ranges = data[-half_idx:] - data[:half_idx]
    smallest_range_idx = np.argmin(ranges)

    # Now repeat the procedure on the half that spans the smallest range
    subset = data[smallest_range_idx: (smallest_range_idx + half_idx)]

    return half_sample_mode(subset, sorted=True)


def asymetric_distance(positions_a, positions_b):
    shape_a = positions_a.shape
    shape_b = positions_b.shape
    distance = np.zeros(shape_a[0])

    for i, pos in enumerate(positions_a):
        distance[i] = np.linalg.norm((pos - positions_b), axis=1).min()

    return distance


def difference(reference, test):
    if reference.shape[1] != test.shape[1]:
        raise ValueError("'reference' and 'test' must have same shapes for all axis > 1.")
    difference = np.zeros(reference.shape)

    for i, pos in enumerate(reference):
        min_idx = np.argmin(np.linalg.norm((pos - test), axis=1))
        difference[i] = pos - test[min_idx]

    return difference


output_path = Path("metrica_interindividuo").resolve()
output_path.mkdir(exist_ok=True)

# path_a = Path("fitness_estadistica/cov/0/individual.npz").resolve()
path_a = Path("SAMPLE_TWOLINES/200331-160718_matlab_background=0/sample_generator.npz")
base_path_b = Path("fitness_estadistica/cov/").resolve()
paths_b = np.array([Path(x).resolve() for x in base_path_b.iterdir() if x.is_dir()])
paths_b = paths_b[np.argsort([float(x.name) for x in paths_b])]  # Ordeno por numero de iteracion

psf_sigma = np.sqrt(2.0592)

fig_titles = {"ab": "True - Individuals",
              "ba": "Individuals - True"}
colors = {"ab": "#21c499",
          "ba": "#f76e48"}

# Cargo individuo A
positions_a = np.load(path_a)["positions"]
if positions_a.ndim == 3:
    positions_a = positions_a[-1, :, :]

# Cargo todas las posiciones de individuos B
positions_b = []
for path_b in paths_b:
    data = np.load(path_b / "individual.npz")
    positions_b.append(data["positions"])
    generation = data["generation"]
    data.close()
positions_b = np.array(positions_b)

# Analizo
differences = []

for iter_idx in [generation.size - 1]:
    print(f"Procesando generación no {iter_idx} de {generation.size}")

    for pos in positions_b:
        differences.append(difference(pos[iter_idx], positions_a))

differences = np.concatenate(differences, axis=0)

mean = np.mean(differences, axis=0)
rel_mean = mean / psf_sigma
cov = np.cov(differences, rowvar=False)
var, vec = np.linalg.eig(cov)
std = np.sqrt(var)
rel_std = std / psf_sigma
angle = np.arctan(vec[1, 0] / vec[0, 0])

rotation_m = vec.T
rotated = (rotation_m @ (differences - mean).T).T / psf_sigma

print(f"cov = \n"
      f"{cov}\n\n"
      f"mean = {mean}\n"
      f"variances = {var}\n"
      f"angle = {np.rad2deg(angle)}°\n"
      f"v1*v2 = {vec[:, 0] @ vec[:, 1]}")

# Figura
left, width = 0.18, 0.6
bottom, height = 0.1, 0.6
hist_height = 0.2
spacing = 0.005

rect_scatter = [left, bottom, width, height]
rect_histx = [left, bottom + height + spacing, width, hist_height]
rect_histy = [left + width + spacing, bottom, hist_height, height]

std_max = np.max(rel_std)
xlim = mean[0] + 5 * np.array([-std_max, std_max])
ylim = mean[1] + 5 * np.array([-std_max, std_max])

fig = plt.figure(figsize=(6, 6))
ax: plt.Axes = fig.add_axes(rect_scatter)
ax_histx = fig.add_axes(rect_histx, sharex=ax)
ax_histy = fig.add_axes(rect_histy, sharey=ax)

ax.scatter(rotated[:, 0], rotated[:, 1], s=1, color="k", linewidth=0, alpha=0.2)
xytext = ["x", "y"]
for i, (a, v) in enumerate(zip(rel_std, vec.T / psf_sigma)):
    v_rotated = (rotation_m @ v.T).T
    xy_rotated = rotation_m.T / psf_sigma
    # print((v @ rotation_m @ np.array([[1, 0]]).T))
    # print((v @ rotation_m @ np.array([[0, 1]]).T))
    print(f"v{i} = {v}\n"
          f"a * v{i} rotated = {a * v_rotated}\n"
          f"{xytext[i]} rotated = {xy_rotated[i]}")

    ax.annotate(s="", xy=(0, 0), xytext=a * v_rotated, color="#DD2244",
                arrowprops=dict(arrowstyle="<-", color="#DD2244"))
    ax.text(x=a*v_rotated[0]*1.1, y=a*v_rotated[1]*1.1, s=f"v{i}", color="#DD2244", fontsize=10)
    ax.annotate(s="", xy=(0, 0), xytext=xy_rotated[i], color="#4422DD",
                arrowprops=dict(arrowstyle="<-", color="#4422DD"))
    ax.text(x=xy_rotated[i][0]*1.1, y=xy_rotated[i][1]*1.1, s=f"{xytext[i]}", color="#4422DD", fontsize=10)
ax.text(0.01, 0.99, s=f"mean / psf_sigma = [{', '.join([f'{x:.2e}' for x in rel_mean])}]\n"
                      f"std / psf_sigma = [{', '.join([f'{x:.2e}' for x in rel_std])}]\n"
                      f"angle = {np.rad2deg(angle):.1f}°",
        horizontalalignment="left", verticalalignment="top", transform=ax.transAxes,
        fontsize=8)

ax_histy: plt.Axes
ax_histx.hist(rotated[:, 0], bins="fd", linewidth=0, color="#999999")
ax_histx.set_yscale("log")
ax_histx.tick_params(axis="x", labelbottom=False)
ax_histx.set_ylim(bottom=0.5)
# ax_histx.set_axis_off()

ax_histy.hist(rotated[:, 1], bins="fd", linewidth=0, orientation='horizontal', color="#999999")
ax_histy.set_xscale("log")
ax_histy.tick_params(axis="y", labelleft=False)
ax_histy.set_xlim(left=0.5)
# ax_histy.set_axis_off()

ax.set_xlim(xlim)
ax.set_ylim(ylim)
ax.set_xlabel("x' / psf_sigma")
ax.set_ylabel("y' / psf_sigma")
ax.grid(which="both", ls=":")

fig: plt.Figure
fig.suptitle("Diferencia individuo fintal vs ground truth")
fig.savefig(output_path / "diferencias_scatter.png")
plt.show()
