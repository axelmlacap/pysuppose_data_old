#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from pathlib import Path


def half_sample_mode(data: np.ndarray, sorted: bool = False):
    if not sorted:
        data = np.sort(data)

    if len(data) == 1:
        return data
    if len(data) == 2:
        return np.mean(data)
    if len(data) == 3:
        if data[1] - data[0] < data[2] - data[1]:
            return np.mean(data[0:1])
        elif data[1] - data[0] > data[2] - data[1]:
            return np.mean(data[1:2])
        elif data[1] - data[0] == data[2] - data[1]:
            return data[1]

    half_idx = int((len(data) + 1) / 2)  # Redondea para incluir el valor intermedio, en el caso de una cantidad impar
    # de elementos

    # Calculate all interesting ranges that span half of all data points
    ranges = data[-half_idx:] - data[:half_idx]
    smallest_range_idx = np.argmin(ranges)

    # Now repeat the procedure on the half that spans the smallest range
    subset = data[smallest_range_idx: (smallest_range_idx + half_idx)]

    return half_sample_mode(subset, sorted=True)


def asymetric_distance(positions_a, positions_b):
    shape_a = positions_a.shape
    shape_b = positions_b.shape
    distance = np.zeros(shape_a[0])

    for i, pos in enumerate(positions_a):
        distance[i] = np.linalg.norm((pos - positions_b), axis=1).min()
        means[key][iter_idx] = distances[key].mean()
        modes[key][iter_idx] = half_sample_mode(distance[key])

    return distance


output_path = Path("metrica_interindividuo").resolve()
output_path.mkdir(exist_ok=True)

# path_a = Path("fitness_estadistica/cov/0/individual.npz").resolve()
path_a = Path("SAMPLE_TWOLINES/200331-160718_matlab_background=0/sample_generator.npz")
base_path_b = Path("fitness_estadistica/cov/").resolve()
paths_b = np.array([Path(x).resolve() for x in base_path_b.iterdir() if x.is_dir()])
paths_b = paths_b[np.argsort([float(x.name) for x in paths_b])]  # Ordeno por numero de iteracion


fig_titles = {"ab": "True - Individuals",
              "ba": "Individuals - True"}
colors = {"ab": "#21c499",
          "ba": "#f76e48"}

# # Cargo individuo A
# positions_a = np.load(path_a)["positions"]
# if positions_a.ndim == 3:
#     positions_a = positions_a[-1, :, :]
#
# # Cargo todas las posiciones de individuos B
# positions_b = []
# for path_b in paths_b:
#     data = np.load(path_b / "individual.npz")
#     positions_b.append(data["positions"])
#     generation = data["generation"]
#     data.close()
# positions_b = np.array(positions_b)
#
# # Analizo
# distances = {key: [] for key in ("ab", "ba")}
# means = {key: np.zeros(positions_b.shape[1]) for key in ("ab", "ba")}
# modes = {key: np.zeros(positions_b.shape[1]) for key in ("ab", "ba")}
#
# for iter_idx in range(generation.size):
#     print(f"Procesando generación no {iter_idx} de {generation.size}")
#     distance = {key: np.array([]) for key in ("ab", "ba")}
#
#     for file_idx in range(positions_b.shape[0]):
#         distance["ab"] = np.append(distance["ab"], asymetric_distance(positions_a, positions_b[file_idx, iter_idx]))
#         distance["ba"] = np.append(distance["ba"], asymetric_distance(positions_b[file_idx, iter_idx], positions_a))
#
#     for i, key in enumerate(["ab", "ba"]):
#         distances[key].append(distance[key])
#
# for i, key in enumerate(["ab", "ba"]):
#     distances[key] = np.array(distances[key])
#
# # Guardo datos
# data_to_save = {"positions_a": positions_a,
#                 "positions_b": positions_b,
#                 "generation": generation}
# np.savez(output_path / "positions.npz", **data_to_save)
# np.savez(output_path / "distances.npz", **distances)
# np.savez(output_path / "means.npz", **means)
# np.savez(output_path / "modes.npz", **modes)


# Cargo datos
data = np.load(output_path / "positions.npz")
generation = data["generation"]
distances = np.load(output_path / "distances.npz")
means = np.load(output_path / "means.npz")
modes = np.load(output_path / "modes.npz")

stds = {key: np.zeros(generation.size) for key in ("ab", "ba")}

for key in ("ab", "ba"):
    stds[key][:] = distances[key].std(axis=1)


xlim_min = min([distances[key].min() for key in distances.keys()]) * 0.9
xlim_max = max([distances[key].max() for key in distances.keys()]) * 1.1

for iter_idx in range(generation.size // 3 + 1):
    iter_idx = min(iter_idx * 3, generation.size - 1)
    print(f"Graficando generación no {iter_idx} de {generation.size}")

    fig, ax = plt.subplots(nrows=2, sharex=True, figsize=(6, 4))
    for i, key in enumerate(["ab", "ba"]):
        ax[i].hist(distances[key][iter_idx], bins="fd", linewidth=1, facecolor=colors[key] + "77",
                   edgecolor=colors[key], label=fig_titles[key])
        ax[i].axvline(means[key][iter_idx], color="black", ls="-", linewidth=1,
                      label=f"Mean = {means[key][iter_idx]:.3e}")
        ax[i].axvline(modes[key][iter_idx], color="black", ls="--", linewidth=1,
                      label=f"Mode = {modes[key][iter_idx]:.3e}")
        ax[i].set_ylabel(f"Cuentas (N = {distances[key][iter_idx].size})")
        ax[i].legend(loc="upper left", bbox_to_anchor=(1, 1), fontsize=8)
        ax[i].set_xlim(xlim_min, xlim_max)
        # ax[i].text(s=f"n = {distance.size}\n" f"mean = {means[iter_idx]:.3e}\n" f"mode = {modes[iter_idx]:.3e}",
        # x=0.99, y=0.99, horizontalalignment="right", verticalalignment="top", transform=ax.transAxes, fontsize=8)
    ax[0].set_title(f"Generación {generation[iter_idx]}")
    ax[0].set_xscale("log")
    ax[1].set_xlabel("Distancia [px]")
    fig.tight_layout()
    fig.savefig(output_path / f"histograma_{generation[iter_idx]}.png")
    plt.close()


# Figura de evolución
fig, ax = plt.subplots(figsize=(7, 4))
for key in ["ab", "ba"]:
    ax.plot(generation, means[key], color=colors[key], ls="-", linewidth=1, label=f"Mean ({fig_titles[key]})")
    ax.plot(generation, modes[key], color=colors[key], ls="--", linewidth=1, label=f"Mode ({fig_titles[key]})")
    ax.fill_between(generation, means[key], means[key] + stds[key], color=colors[key], alpha=0.15,
                    linewidth=0, label="Mean + Std")
ax.set_xscale("log")
ax.set_yscale("log")
ax.set_ylabel("Distancia [px]")
ax.set_xlabel("Generación")
ax.grid(ls=":")
ax.legend(loc="upper left", bbox_to_anchor=(1, 1), fontsize=8)
plt.tick_params(which="both", left=True, right=True, direction="in")
fig.tight_layout()
fig.savefig(output_path / "evolucion.png")

# plt.figure()
# plt.scatter(positions_a[:, 0], positions_a[:, 1])
#
# plt.figure()
# plt.scatter(positions_b[:, 0], positions_b[:, 1])
#
# plt.show()
#
# plt.figure()
# plt.plot(distance)
#
# plt.figure()
# plt.hist(distance, bins="fd")
# plt.show()
