#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from pathlib import Path


def half_sample_mode(data: np.ndarray, sorted: bool = False):
    if not sorted:
        data = np.sort(data)

    if len(data) == 1:
        return data
    if len(data) == 2:
        return np.mean(data)
    if len(data) == 3:
        if data[1] - data[0] < data[2] - data[1]:
            return np.mean(data[0:1])
        elif data[1] - data[0] > data[2] - data[1]:
            return np.mean(data[1:2])
        elif data[1] - data[0] == data[2] - data[1]:
            return data[1]

    half_idx = int((len(data) + 1) / 2)  # Redondea para incluir el valor intermedio, en el caso de una cantidad impar
    # de elementos

    # Calculate all interesting ranges that span half of all data points
    ranges = data[-half_idx:] - data[:half_idx]
    smallest_range_idx = np.argmin(ranges)

    # Now repeat the procedure on the half that spans the smallest range
    subset = data[smallest_range_idx: (smallest_range_idx + half_idx)]

    return half_sample_mode(subset, sorted=True)


def asymetric_distance(positions_a, positions_b):
    shape_a = positions_a.shape
    shape_b = positions_b.shape
    distance = np.zeros(shape_a[0])

    for i, pos in enumerate(positions_a):
        distance[i] = np.linalg.norm((pos - positions_b), axis=1).min()

    return distance


output_path = Path("metrica_interindividuo_intrasuppose").resolve()
output_path.mkdir(exist_ok=True)


colors = {"chi": "#21c499", "cov": "#f76e48"}

fitness = ["chi", "cov"]

for fit in fitness:
    base_path = Path(f"fitness_estadistica/{fit}/").resolve()
    paths = np.array([Path(x).resolve() for x in base_path.iterdir() if x.is_dir()])
    paths = paths[np.argsort([float(x.name) for x in paths])]  # Ordeno por numero de iteracion
    # paths = paths[0:2]
    n_runs = paths.size


    # Cargo todas las posiciones de individuos finales
    positions = []
    idx_to_get = -1
    for path in paths:
        data = np.load(path / "individual.npz")
        positions.append(data["positions"][idx_to_get])
        generation = data["generation"][idx_to_get]
        data.close()
    positions = np.array(positions)

    # Analizo
    distances = []

    for idx_a in range(n_runs):
        print(f"Procesando individuo no {idx_a} de {n_runs}")
        pos_a = positions[idx_a]
        for idx_b in range(idx_a + 1, n_runs):
            pos_b = positions[idx_b]
            distances = np.append(distances, asymetric_distance(pos_a, pos_b))

    distances = np.array(distances)
    mean = np.mean(distances)
    mode = half_sample_mode(distances)
    std = np.std(distances)

    # Guardo datos
    np.savez(output_path / f"distancias_{fit}.npz", distances=distances, mean=mean, mode=mode, std=std)

    # # Cargo datos
    #

    fig, ax = plt.subplots()
    ax.hist(distances, bins="fd", linewidth=1, facecolor=colors[fit] + "77", edgecolor=colors[fit],
            label=f"Histograma\n"
                  f"N={distances.size}\n"
                  f"Gen={generation}")
    ax.axvline(mean, color="black", ls="-", linewidth=0.5, label=f"Mean = {mean:.3e}")
    ax.axvline(mode, color="black", ls="--", linewidth=0.5, label=f"Mode = {mode:.3e}")
    y = np.mean(ax.get_ylim())
    ax.plot([mean-std, mean+std], [y, y], color="black", linewidth=0.5, marker="o", markersize=2,
            label=f"Std = {std:.3e}")
    ax.set_ylabel(f"Cuentas")
    ax.legend(fontsize=8)
    # ax[i].text(s=f"n = {distance.size}\n" f"mean = {means[iter_idx]:.3e}\n" f"mode = {modes[iter_idx]:.3e}",
    # x=0.99, y=0.99, horizontalalignment="right", verticalalignment="top", transform=ax.transAxes, fontsize=8)
    ax.set_title(f"Histograma de distancia entre fuentes, fitness = {fit}")
    ax.set_xscale("log")
    ax.set_xlabel("Distancia [px]")
    fig.tight_layout()
    fig.savefig(output_path / f"histograma_{fit}.png")
    plt.close()
