import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import matplotlib

from pathlib import Path

import time

from pysuppose.bases.psf import GaussianPSF
from pysuppose.bases.device import CPU, GPU
from pysuppose.bases.sample import Sample
from pysuppose.engine import correlation_fitness, fitness_function
from pysuppose.bases.individual import Individual


output_path = Path("fitness_timing/").resolve()
output_path.mkdir(exist_ok=True)

n_iter = 100
image_sizes = np.linspace(8, 128, 10, dtype=int)
# population_sizes = np.linspace(10, 500, 10, dtype=int)
n_sources = np.linspace(50, 1000, 10, dtype=int)

# Inicializo variables
shape = (image_sizes.size, n_sources.size)

# times = {"convolution": np.zeros(shape),
#          "chi": np.zeros(shape),
#          "cov": np.zeros(shape)}
#
# psf = GaussianPSF(amplitude=1, sigma=1, offset=0)
#
# gpu = GPU(0)
#
# for i, image_size in enumerate(image_sizes):
#     image_shape = (image_size, image_size)
#     gpu.set_output_shape(image_shape)
#     sample = np.random.randn(*image_shape)
#
#     for j, n in enumerate(n_sources):
#         positions = np.random.uniform(0, image_size, size=(n, 2))
#         individual = Individual(positions, alpha=1.0)
#
#         for k in range(n_iter):
#             start = time.perf_counter()
#             convolved = gpu.convolve(individual, psf)
#             end = time.perf_counter()
#             times["convolution"][i, j] += (end - start) / n_iter
#
#             start = time.perf_counter()
#             fitness_function(None, convolved, sample)
#             end = time.perf_counter()
#             times["chi"][i, j] += (end - start) / n_iter
#
#             start = time.perf_counter()
#             correlation_fitness(None, convolved, sample)
#             end = time.perf_counter()
#             times["cov"][i, j] += (end - start) / n_iter
#
# np.savez(output_path / "times.npz", **times)

times = np.load(output_path / "times.npz")

print(f"Convolution / Chi:\n"
      f"    min = {(times['convolution'] / times['chi']).min()}\n"
      f"    max = {(times['convolution'] / times['chi']).max()}\n"
      f"    mean = {(times['convolution'] / times['chi']).mean()}\n"
      f"    std = {(times['convolution'] / times['chi']).std()}\n")

print(f"Convolution / cov:\n"
      f"    min = {(times['convolution'] / times['cov']).min()}\n"
      f"    max = {(times['convolution'] / times['cov']).max()}\n"
      f"    mean = {(times['convolution'] / times['cov']).mean()}\n"
      f"    std = {(times['convolution'] / times['cov']).std()}\n")

print(f"cov / chi:\n"
      f"    min = {(times['cov'] / times['chi']).min()}\n"
      f"    max = {(times['cov'] / times['chi']).max()}\n"
      f"    mean = {(times['cov'] / times['chi']).mean()}\n"
      f"    std = {(times['cov'] / times['chi']).std()}\n")

# Figuras individuales
fig, ax = plt.subplots(nrows=3, sharex="all", figsize=(6, 3*3))
fig.suptitle("Time vs number of sources")
for key_idx, key in enumerate(["convolution", "chi", "cov"]):
    for i, size in enumerate(image_sizes):
        ax[key_idx].plot(n_sources, times[key][i, :] * 1e6, marker="o", markersize=3, markeredgecolor="none", ls=":",
                         linewidth=0.5, label=f"{size}x{size} px")

    ax[key_idx].set_title(key.capitalize())
    ax[key_idx].set_ylabel("Time [us]")
    ax[key_idx].grid(axis="both", ls=":")
    if key_idx == 0:
        ax[key_idx].legend(bbox_to_anchor=(1.01, 1), loc="upper left")
ax[key_idx].set_xlabel("Number of sources")
fig.tight_layout()
plt.subplots_adjust(top=0.9, bottom=0.1)
fig.savefig(output_path / "time_vs_nsources.png")


fig, ax = plt.subplots(nrows=3, sharex="all", figsize=(6, 3*3))
fig.suptitle("Time vs image size")
for key_idx, key in enumerate(["convolution", "chi", "cov"]):
    for j, n in enumerate(n_sources):
        ax[key_idx].plot(image_sizes, times[key][:, j] * 1e6, marker="o", markersize=3, markeredgecolor="none", ls=":",
                         linewidth=0.5, label=f"{n} sources")
    ax[key_idx].set_title(key.capitalize())
    ax[key_idx].set_ylabel("Time [us]")
    ax[key_idx].grid(axis="both", ls=":")
    if key_idx == 0:
        ax[key_idx].legend(bbox_to_anchor=(1.01, 1), loc="upper left")
ax[key_idx].set_xlabel("Image size [px]")
fig.tight_layout()
plt.subplots_adjust(top=0.9, bottom=0.1)
fig.savefig(output_path / "time_vs_image_sizea.png")

plt.show()