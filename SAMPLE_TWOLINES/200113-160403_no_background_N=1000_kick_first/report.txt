
PSF:
    psf_type: expression2d
    function_name: gaussian2D (id:0)
    amplitude: 0.0773
    sigma2: 2.0592
    offset: 0.0000
    area (64x64): 1.000e+00
    mean (64x64): 2.441e-04

PimpedTwoLines:
    n_lines: 2
    lines_sep: 2.1238e+00
    lines_centre: [-1.0619  1.0619]
    sources_per_line: 13.0
    sources_sep: 1.4142e+00
    angle: 7.8540e-01
    alpha: 4.0000e+04
    background: 0.0000e+00
    noise_type: mixed
    noise_std: 2.3000e+01

SimpleIndividualGenerator:
    alpha: 2500.0
    alpha / max(sample): 0.20645721946872508
    subtract_mean: False
    iterations: 5000
    mode: from_image
    window_size: 1
    kick_radius: 1.435
    template: kick_first
