#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
import time

from pysuppose.bases.psf import GaussianPSF
from pysuppose.sample_generators.twolines import PimpedTwoLines


# Información de la corrida
SAMPLE_FOLDER = 'SAMPLE_TWOLINES'
SAMPLE_NAME = "matlab_background=20000"
plot_result = True

# Generación de la ruta de guardado (se puede reemplazar por un path específico)
timestamp = time.strftime('%y%m%d-%H%M%S')
MAIN_PATH = Path(__file__).resolve().parent
sample_path = Path(MAIN_PATH / SAMPLE_FOLDER).resolve()
run_path = (sample_path / (timestamp + ("_" + SAMPLE_NAME if SAMPLE_NAME != "" else ""))).resolve()
run_path.mkdir(exist_ok=True, parents=True)  # Crea la ruta completa


# Inicializo PSF
psf = GaussianPSF()
psf.sigma = 1.435
psf.offset = 0
psf.amplitude = 1/(2*np.pi*psf.sigma**2)


# Inicializo muestra
generator = PimpedTwoLines(n_lines=2,
                           lines_sep=psf.sigma*1.4783,
                           sources_per_line=71,
                           sources_sep=np.sqrt(2)*7/71,
                           angle=np.deg2rad(45),
                           alpha=15000,
                           background=20000,
                           # background=0,
                           noise_type="mixed",
                           noise_std=23,
                           image_shape=(25, 25),
                           psf=psf)
sample = generator.get_sample(subtract_mean=False)
sample.save(run_path / "sample.npz")
generator.save(run_path / "sample_generator.npz")


# Grafico la muestra
plt.figure()
plt.imshow(sample.full_data)
plt.colorbar()
plt.title("Muestra original")
plt.savefig(run_path / "sample.png")


if plot_result:
    plt.show()


# Genero reporte
report = str(psf) + str(generator)
print(report)
with open(run_path / "report.txt", "a+") as file:
    file.write(report)
