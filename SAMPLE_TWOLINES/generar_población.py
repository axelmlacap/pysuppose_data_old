#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
import time
import yaml

from pysuppose.bases.individual_generator import SimpleIndividualGenerator
from pysuppose.bases.psf import GaussianPSF
from pysuppose.bases.sample import Sample


# Información de la corrida
run_name = "multiplo_exacto_N=100_kick_first"
sample_folder = 'SAMPLE_TWOLINES'
sample_name = '200331-160718_matlab_background=0'
plot_result = True
subtract_mean = False


# Cargo la muestra
timestamp = time.strftime('%y%m%d-%H%M%S')
population_name = "_".join(["population", run_name])
MAIN_PATH = Path(__file__).resolve().parent
sample_path = Path(MAIN_PATH / sample_folder / sample_name).resolve()
sample = Sample.load(sample_path / "sample.npz", subtract_mean=subtract_mean)
subtract_mean = sample.mean == 0


# Inicializo PSF
with open(sample_path / "report.txt", "r+") as file:
    psf_dict = yaml.load(file, Loader=yaml.SafeLoader)["PSF"]

sigma = np.sqrt(psf_dict["sigma2"])*1.1
amplitude = 1/np.sqrt(2*np.pi*sigma**2)
psf = GaussianPSF(amplitude=amplitude, sigma=sigma, offset=psf_dict["offset"])


# Genero población

n_sources = 426
psf_integral = psf.integrate(sample.roi_shape)
alpha = float(np.sum(sample.roi_data)/(n_sources*psf_integral)) - 0.1/n_sources

generator = SimpleIndividualGenerator(sample=sample,
                                      psf=psf,
                                      alpha=alpha,
                                      subtract_mean=subtract_mean,
                                      iterations=5000,
                                      mode="from_image",
                                      window_size=1)

population = generator.generate_population(size=100,
                                           elite=10,
                                           kick_radius=psf.sigma,
                                           template="kick_first")


# Gráfico
generator.plot_result(path=sample_path / (population_name + "_prueba.png"))


# Guardo
# population.export_generation_hdf5(sample_path / (population_name + ".hdf5"))


# Genero reporte
report = str(generator)
print(report)
with open(sample_path / "report.txt", "a+") as file:
    file.write(report)
