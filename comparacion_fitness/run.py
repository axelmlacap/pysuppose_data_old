#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from pathlib import Path
import time
import yaml

from pysuppose import SupposeEngine
from pysuppose.engine import fitness_function, correlation_fitness, after_convolve_recalculate_alpha, \
    after_convolve_subtract_mean, evolve_function
import pysuppose.ga_operators.matlab_ga_operators as matlab_ga_operators
import pysuppose.ga_operators.standard_ga_operators as standard_ga_operators
import pysuppose.ga_operators.alternative_ga_operators as alternative_ga_operators
from pysuppose.bases.psf import GaussianPSF, FunctionPSF
from pysuppose.bases.device import CPU, GPU
from pysuppose.bases.population import Population
from pysuppose.bases.sample import Sample
from pysuppose.bases.individual import Individual
from pysuppose.sample_generators.twolines import PimpedTwoLines
from pysuppose.addons.metrics import FitnessMetrics, DiversityMetrics, TwoLinesMetrics, SelectionMonitor
from pysuppose.addons.plots import SimplePlot, AttributePlot, ConvolvedMonitor
from pysuppose.addons.report import ReportAddOn
from pysuppose.addons.save import SaveArray, SaveHDF5, SavePopulation
from pysuppose.addons.adaptation import ControlLoopAdaptation
from pysuppose.bases.individual_generator import SimpleIndividualGenerator

import logging

logging.getLogger('pysuppose').setLevel('DEBUG')

# Información de la corrida
# Nombre
USERNAME = 'Axel'
PROJECT = 'fitness2'
RUN = "corr_con_fondo_30000iter"

# Muestra
sample_folder = "SAMPLE_TWOLINES"
sample_name = "200331-160811_matlab_background=20000"
subtract_mean = True

# Población
population_folder = sample_folder + "/" + sample_name
population_name = "population_norma_minima_N=100_kick_first"  # "last_population.hdf5"
elite = 10

# Ejecución
gpu_number = 0
n_iterations = 30000
addon_interval = 1000
addon_interval_mode = "logspace"
plot_interval = 1000
plot_interval_mode = "logspace"

# Algoritmo
fitness = correlation_fitness
evolution = evolve_function
selection = matlab_ga_operators.select
# selection = matlab_ga_operators.select
cross = matlab_ga_operators.cross
mutation = matlab_ga_operators.mutate

# Addons
best_individual_addons = True
last_population_addons = True
basic_fitness_addons = True
advanced_fitness_addons = True
diversity_metrics_addons = False
twolines_metrics_addons = True
selection_monitor_addons = False
convolved_monitor_addons = False
auxiliar_addons = False

# Miscelánea
plot = False

# Generación de la ruta de guardado
MAIN_PATH = Path(__file__).resolve().parent

timestamp = time.strftime('%y%m%d-%H%M%S')
project_path = Path(MAIN_PATH / PROJECT).resolve()
run_path = (project_path / (timestamp + ("_" + RUN if RUN != "" else ""))).resolve()
run_path.mkdir(exist_ok=True, parents=True)  # Crea la ruta completa
sample_path = Path(MAIN_PATH / sample_folder / sample_name).resolve()
population_path = Path(MAIN_PATH / population_folder / population_name).resolve()

# Carga muestra, PSF e individuos iniciales
with open(sample_path / "report.txt", "r+") as file:
    report_dict = yaml.load(file, Loader=yaml.SafeLoader)

psf_dict = report_dict["PSF"]
psf_dict.update({"sigma": np.sqrt(psf_dict["sigma2"])})
sample_generator_dict = PimpedTwoLines.load(sample_path / "sample_generator.npz").to_dict()
population_generator_dict = report_dict["SimpleIndividualGenerator"]

# Cargo PSF
psf = GaussianPSF(amplitude=psf_dict["amplitude"], sigma=psf_dict["sigma"], offset=psf_dict["offset"])

# Cargo muestra
sample = Sample.load(sample_path / "sample.npz", subtract_mean=subtract_mean)
sample.save(run_path / "sample.npz")

if plot:
    # Grafico la muestra
    plt.figure()
    plt.imshow(sample.full_data)
    plt.colorbar()
    plt.title("Muestra original")
    plt.show()

# Cargo población y redefino elite
population = Population.import_generation_hdf5(population_path.with_suffix(".hdf5"))
population._elite = elite

# # Pateo individuos
# best_individual = population.best_individual.clone()
# mutated = mutate(population.current_generation, mutate_percent=1, mutate_radius=psf.sigma*0.1)
# population = Population(population.size, population.sort_descending, population.elite)
# population._new_generation = [best_individual] + mutated[:-1]
# population.export_generation_hdf5(run_path / "initial_population.hdf5")


# Inicializo el engine que voy a usar
engine = SupposeEngine(sample=sample, psf=psf, population=population)
engine.resource_manager.add_device(GPU(gpu_number))

if subtract_mean:
    engine.register_as_checkpoint(after_convolve_subtract_mean)  # Resta valor medio
engine.register_as_checkpoint(after_convolve_recalculate_alpha)  # Recalcula alpha

# Configuro parámetros del algoritmo genético
# Fitness
engine.register_as_fitness(fitness)

# Evolución
engine.register_as_evolve(evolution)

# Selección
engine.register_as_selection(selection)

# Cruza
engine.register_as_cross(cross)
engine.crossover.cross_percent = 0.4  # Porcentaje de cruzas realizadas

# Mutación
engine.register_as_mutation(mutation)
engine.mutation.mutate_percent = 0.2
engine.mutation.mutate_radius = 0.01 * psf.sigma


# Autoguardado
engine.auto_save_path = run_path / 'autosave_population.hdf5'
engine.auto_save_time = 300

# Inicializo addons
# Reporte
report = ReportAddOn(interval_value=addon_interval,
                     interval_mode=addon_interval_mode)
report.setup_console()
report.setup_file(run_path / f'report.txt')


# Configuración de addons
cmap = cm.get_cmap("tab10")

# Mejor individuo
simple_plot = SimplePlot(interval_value=addon_interval,
                         interval_mode=addon_interval_mode,
                         path=run_path / 'mejor_individuo.png',
                         file_per_interval=True)

save_best_individual = SaveArray(attributes={'positions': 'engine.population.best_individual.positions',
                                             'alpha': 'engine.population.best_individual.alpha',
                                             'fitness': 'engine.population.best_individual.fitness',
                                             'convolved': 'engine.population.best_individual.convolved'},
                                 path=run_path / 'individual.npz',
                                 file_per_interval=False,
                                 interval_value=addon_interval,
                                 interval_mode=addon_interval_mode)

# Última población
save_population = SavePopulation(path=run_path / "last_population.hdf5",
                                 file_per_interval=False,
                                 interval_value=addon_interval,
                                 interval_mode=addon_interval_mode)

# Métricas de fitness
fitness_metrics = FitnessMetrics(noise_pattern=sample_generator_dict["noise_pattern"],
                                 slope_window=100,
                                 additional_fitness=[fitness_function, correlation_fitness],
                                 store=True,
                                 interval_value=addon_interval,
                                 interval_mode=addon_interval_mode)

plot_absolute_fitness = AttributePlot({"Mejor fitness": "engine.population.best_fitness",
                                       "Promedio": "engine.population.mean_fitness",
                                       "Std": "engine.population.std_fitness"},
                                      axes_directives=[{"xscale": "log", "yscale": "log", "linecolor": cmap(0)},
                                                       {"xscale": "log", "yscale": "log", "linecolor": cmap(1)},
                                                       {"xscale": "log", "yscale": "log", "linecolor": cmap(2)}],
                                      path=run_path / 'fitness_absolute.png',
                                      file_per_interval=False,
                                      interval_value=plot_interval,
                                      interval_mode=plot_interval_mode)

plot_relative_fitness = AttributePlot({"Mejor / ideal": "engine.population.relative_fitness",
                                       "Std / Promedio": "engine.population.relative_dispersion"},
                                      axes_directives={"xscale": "log", "yscale": "log"},
                                      path=run_path / 'fitness_relative.png',
                                      file_per_interval=False,
                                      interval_value=plot_interval,
                                      interval_mode=plot_interval_mode)

save_fitness_metrics = SaveArray(attributes={"best_fitness": "engine.population.best_fitness",
                                             "worst_fitness": "engine.population.worst_fitness",
                                             "mean_fitness": "engine.population.mean_fitness",
                                             "variability": "engine.population.variability",
                                             "ptp_dispersion": "engine.population.ptp_dispersion",
                                             "std_fitness": "engine.population.std_fitness",
                                             "ideal_fitness": "engine.population.ideal_fitness",
                                             "relative_fitness": "engine.population.relative_fitness",
                                             "relative_dispersion": "engine.population.relative_dispersion",
                                             "fitness_slope": "engine.population.fitness_slope",
                                             "fitness_logslope": "engine.population.fitness_logslope",
                                             "fitness": "engine.population.fitness",
                                             "chi": "engine.population.fitness_function",
                                             "correlation": "engine.population.correlation_fitness"
                                             },
                                 path=run_path / "fitness.npz",
                                 file_per_interval=False,
                                 interval_value=addon_interval,
                                 interval_mode=addon_interval_mode)

# Métricas de dos rectas
twolines_metrics = TwoLinesMetrics(true_centers=sample_generator_dict["lines_centre"],
                                   angle=sample_generator_dict["angle"],
                                   true_sigma=psf_dict["sigma"],
                                   sample=sample_generator_dict["image"],
                                   true_sample=sample_generator_dict["true_image"],
                                   im_shape=sample.roi_shape,
                                   plot=True,
                                   path=run_path / "likelyhood_fit.png",
                                   file_per_interval=True,
                                   interval_value=addon_interval,
                                   interval_mode=addon_interval_mode)

plot_twolines_metrics = AttributePlot({"Normalized bias": "engine.population.mean_normalized_bias",
                                       "Improvement": "engine.population.mean_improvement"},
                                      path=run_path / 'twolines_attr.png',
                                      axes_directives=[{"xscale": "log", "yscale": "linear", "color": cmap(3)},
                                                       {"xscale": "log", "yscale": "log", "color": cmap(4)}],
                                      file_per_interval=False,
                                      interval_value=plot_interval,
                                      interval_mode=plot_interval_mode)

save_twolines_metrics = SaveArray(attributes={'line_centre': 'engine.population.centre',
                                              'line_bias': 'engine.population.bias',
                                              'line_std': 'engine.population.std',
                                              'normalized_bias': 'engine.population.normalized_bias',
                                              'normalized_std': 'engine.population.normalized_std'},
                                  path=run_path / 'twolines_metrics.npz',
                                  file_per_interval=False,
                                  interval_value=plot_interval,
                                  interval_mode=plot_interval_mode)

# Métricas de diversidad
diversity_metrics = DiversityMetrics(normalize_diversity=True,
                                     interval_value=addon_interval,
                                     interval_mode=addon_interval_mode)

plot_diversity_metrics = AttributePlot({'Population SPD': 'engine.population.population_spd'},
                                       axes_directives={"xscale": "log", "yscale": "log"},
                                       path=run_path / 'diversity.png',
                                       file_per_interval=False,
                                       interval_value=plot_interval,
                                       interval_mode=plot_interval_mode)

save_diversity_metrics = SaveArray(attributes={'mean_individual': 'engine.population.best_fitness',
                                               'center_of_mass': 'engine.population.center_of_mass',
                                               'population_spd': 'engine.population.population_spd',
                                               'individual_spd': 'engine.population.individual_spd',
                                               'best_spd': 'engine.population.best_individual_spd'},
                                   path=run_path / 'diversity.npz',
                                   interval_value=addon_interval,
                                   interval_mode=addon_interval_mode)

# Monitoreo
selection_monitor = SelectionMonitor(plot_style="stack",
                                     store=True,
                                     path=run_path / "selection.png",
                                     file_per_interval=False,
                                     interval_value=plot_interval,
                                     interval_mode=plot_interval_mode)

save_selection = SaveArray(path=run_path / 'selection.npz',
                           attributes={'selection_value': 'engine.population.selection_value'},
                           interval_value=plot_interval,
                           interval_mode=plot_interval_mode)

convolved_monitor = ConvolvedMonitor(path=run_path / "convolved.png",
                                     file_per_interval=True,
                                     interval_value=plot_interval,
                                     interval_mode=plot_interval_mode)

save_convolved = SaveArray(path=run_path / 'convolved.npz',
                           attributes={'convolved': 'engine.population.best_individual.convolved'},
                           interval_value=plot_interval,
                           interval_mode=plot_interval_mode)

# Gráfico auxiliar
# plot_aux = AttributePlot({"Chi": "engine.population.fitness_function",
#                           "Correlation": "engine.population.correlation_fitness"},
#                          axes_directives={"xscale": "log", "yscale": "log"},
#                          path=run_path / 'auxiliar.png',
#                          file_per_interval=False,
#                          interval_value=addon_interval,
#                          interval_mode=addon_interval_mode)
plot_aux = AttributePlot({"Diversity loss": "engine.selection.diversity_loss",
                          "Sel. intensity": "engine.selection.selection_intensity",
                          "Sel. variance": "engine.selection.selection_variance"},
                         axes_directives={"xscale": "log", "yscale": "linear"},
                         path=run_path / 'auxiliar.png',
                         file_per_interval=False,
                         interval_value=plot_interval,
                         interval_mode=plot_interval_mode)

save_aux = SaveArray(attributes={"Diversity loss": "engine.selection.diversity_loss",
                                 "Sel. intensity": "engine.selection.selection_intensity",
                                 "Sel. variance": "engine.selection.selection_variance"},
                     path=run_path / 'auxiliar.npz',
                     file_per_interval=False,
                     interval_value=plot_interval,
                     interval_mode=plot_interval_mode)

engine.connect(report)

if best_individual_addons:
    engine.connect(simple_plot, save_best_individual)
if last_population_addons:
    engine.connect(save_population)
if basic_fitness_addons:
    engine.connect(fitness_metrics, plot_relative_fitness, save_fitness_metrics)
if advanced_fitness_addons and basic_fitness_addons:
    engine.connect(plot_absolute_fitness)
if diversity_metrics_addons:
    engine.connect(diversity_metrics, plot_diversity_metrics, save_diversity_metrics)
if twolines_metrics_addons:
    engine.connect(twolines_metrics, plot_twolines_metrics, save_twolines_metrics)
if selection_monitor_addons:
    engine.connect(selection_monitor, save_selection)
if convolved_monitor_addons:
    engine.connect(convolved_monitor, save_convolved)
if auxiliar_addons:
    engine.connect(plot_aux, save_aux)

# Corro el algoritmo
engine.run(n_iterations)
