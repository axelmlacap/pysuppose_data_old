import numpy as np
from pathlib import Path
import shutil

source = Path("fitness_estadistica/chi_200405-195316").resolve()
files = ["individual.npz", "fitness.npz", "mejor_individuo_30000.png", "likelyhood_fit_30000.png",
         "fitness_absolute.png", "fitness_relative.png", "twolines_attr.png"]

subfolders = np.array([Path(x).resolve() for x in source.iterdir() if x.is_dir()])
subfolders = subfolders[np.argsort([float(x.stem) for x in subfolders])]
destination = Path(str(source) + "_backup").resolve()
destination.mkdir(exist_ok=True)

print(f"Inicio backup de {str(source)}")
for subsource in subfolders:
    print(f"    Carpeta '{str(subsource.stem)}'")
    subdest = (destination / subsource.stem)
    subdest.mkdir(exist_ok=True)

    for file in files:
        print(f"        Copiando en {str(subdest / file)}")

        if file == "individual.npz":
            data = np.load(subsource / file)
            # print([x for x in data.keys()])
            # print(dir(data))
            np.savez(subdest / file, **{key: data[key] for key in data.keys() if key != "convolved"})
        else:
            shutil.copy2(subsource / file, subdest / file, )
