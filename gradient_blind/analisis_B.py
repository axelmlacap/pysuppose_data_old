import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import matplotlib

from pathlib import Path
import yaml

from pysuppose.bases.psf import GaussianPSF
from pysuppose.bases.device import CPU, GPU
from pysuppose.bases.sample import Sample
from pysuppose.engine import correlation_fitness
from pysuppose.bases.individual import Individual

sample_path = Path("SAMPLE_TWOLINES/200331-160718_matlab_background=0").resolve()
data_path = Path("fitness_estadistica/cov/0").resolve()
output_path = Path("prueba_blind_deconvolution/").resolve()
n_sigmas = 1000
psf_factor = np.linspace(0.5, 1.1, n_sigmas)

# # Cargo muestra y PSF
# with open(sample_path / "report.txt", "r+") as file:
#     report_dict = yaml.load(file, Loader=yaml.SafeLoader)
#
# psf_dict = report_dict["PSF"]
# psf_dict.update({"sigma": np.sqrt(psf_dict["sigma2"])})
#
# psf = GaussianPSF(amplitude=psf_dict["amplitude"], sigma=psf_dict["sigma"], offset=psf_dict["offset"])
# sample = Sample.load(sample_path / "sample.npz", subtract_mean=False)
#
# # Cargo datos
# data = np.load(data_path / "individual.npz")
# data_size = data["generation"].size
# n_evaluations = data_size
# indices = np.linspace(0, data_size, n_evaluations, endpoint=False, dtype=int)
#
# # Inicializo GPU
# gpu = GPU(0)
# gpu.set_output_shape(sample.roi_shape)
#
# # Selecciono individuo
# fitness = np.zeros(n_sigmas)
# min = np.zeros(n_evaluations)
#
# for i, index in enumerate(indices):
#     print(f"Iteración {index} de {data_size}")
#     individual = Individual(positions=data["positions"][index],
#                             alpha=data["alpha"][index])
#
#     for j, factor in enumerate(psf_factor):
#         new_sigma = psf.sigma * factor
#         new_amplitude = 1 / np.sqrt(2 * np.pi * new_sigma ** 2)
#         new_offset = psf.offset
#         aux_psf = GaussianPSF(amplitude=new_sigma, sigma=new_amplitude, offset=new_offset)
#         convolved = gpu.convolve(individual, aux_psf)
#         fitness[j] = correlation_fitness(None, convolved, sample.roi_data)
#
#     min[i] = psf_factor[np.argmin(fitness)]
#
# np.savez(output_path / "sigma_min_vs_gen.npz",
#          **{"path": str(data_path / "individual.npz"),
#             "generation": data["generation"],
#             "min": min})

data = np.load(output_path / "sigma_min_vs_gen.npz")
min = data["min"]

# Figura
fig, ax = plt.subplots()
ax.plot(data["generation"], min, linewidth=1, color="k")
plt.xlabel("Iteración")
plt.ylabel("sigma_optimo / sigma_real")
plt.xscale("log")
# plt.yscale("log")
plt.grid(ls=":")
plt.tight_layout()
plt.savefig(output_path / "sigma_min_vs_iteracion.png")
