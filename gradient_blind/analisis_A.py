import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import matplotlib

from pathlib import Path
import yaml

from pysuppose.bases.psf import GaussianPSF
from pysuppose.bases.device import CPU, GPU
from pysuppose.bases.sample import Sample
from pysuppose.engine import correlation_fitness
from pysuppose.bases.individual import Individual


sample_path = Path("SAMPLE_TWOLINES/200331-160718_matlab_background=0").resolve()
data_path = Path("blind_deconvolution/cov/0").resolve()
output_path = Path("prueba_blind_deconvolution/").resolve()
n_sigmas = 500
n_plots = 10
psf_factor = np.logspace(-1, 1, n_sigmas)


# Cargo muestra y PSF
with open(sample_path / "report.txt", "r+") as file:
    report_dict = yaml.load(file, Loader=yaml.SafeLoader)

psf_dict = report_dict["PSF"]
psf_dict.update({"sigma": np.sqrt(psf_dict["sigma2"])})

psf = GaussianPSF(amplitude=psf_dict["amplitude"], sigma=psf_dict["sigma"], offset=psf_dict["offset"])
sample = Sample.load(sample_path / "sample.npz", subtract_mean=False)


# Cargo datos
data = np.load(data_path / "individual.npz")
data_size = data["generation"].size


# Inicializo GPU
gpu = GPU(0)
gpu.set_output_shape(sample.roi_shape)


# Selecciono individuo
indices = np.linspace(0, data_size, n_plots, endpoint=False, dtype=int)
fitness = np.zeros((n_plots, psf_factor.size))
min = np.ones(n_plots)

for i, index in enumerate(indices):
    individual = Individual(positions=data["positions"][index],
                            alpha=data["alpha"][index])

    for j, factor in enumerate(psf_factor):
        new_sigma = psf.sigma * factor
        new_amplitude = 1/(2*np.pi*new_sigma**2)
        new_offset = psf.offset
        aux_psf = GaussianPSF(amplitude=new_amplitude, sigma=new_sigma, offset=new_offset)
        convolved = gpu.convolve(individual, aux_psf)
        fitness[i, j] = correlation_fitness(None, convolved, sample.roi_data)

    min[i] = psf_factor[np.argmin(fitness[i, :].flatten())]


# Figura
fig = plt.figure()
ax = plt.axes(projection='3d')
cmap = matplotlib.cm.get_cmap('viridis')
colors = [cmap(x) for x in np.linspace(0, 1, n_plots)]

for i, index in enumerate(indices):
    ax.plot3D(np.log10(psf_factor),
              np.ones(n_sigmas) * np.log10(data["generation"][index]),
              np.log10(fitness[i, :]),
              linewidth=1,
              color=colors[i])

zlimits = ax.get_zlim()
ax.plot3D(np.log10(min),
          np.log10(data["generation"][indices]),
          np.ones(n_plots) * zlimits[0],
          color="k", linewidth=1)
ax.set_zlim(zlimits)

xticks = [-1, 0, 1]
yticks = [0, 1, 2, 3, 4]
zticks = [-4, -3, -2, -1, 0]
plt.xticks(ticks=xticks, labels=np.power(10.0, xticks))
plt.yticks(ticks=yticks, labels=[f"1E{y}" for y in yticks])
ax.set_zticks(ticks=zticks)
ax.set_zticklabels([f"1E{z}" for z in zticks])
# plt.xticks(ticks=[-1, 0, 1], labels=np.power(10.0, [-1, 0, 1]))

# ax.set_xscale("log")
# ax.set_yscale("log")
# ax.set_zscale("log")
ax.set_xlabel("sigma / true_sigma")
ax.set_ylabel("Generación")
ax.set_zlabel("Fitness")
plt.grid(ls=":")
# plt.text(0.05, 0.95, f"min = {min[-1]:.3e} * psf", {"size": 7}, verticalalignment="top", transform=ax.transAxes)
plt.savefig(output_path / "fitness_vs_psf.png")
