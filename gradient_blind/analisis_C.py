import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import matplotlib

from pathlib import Path
import yaml

from pysuppose.bases.psf import GaussianPSF
from pysuppose.bases.device import CPU, GPU
from pysuppose.bases.sample import Sample
from pysuppose.engine import correlation_fitness
from pysuppose.bases.individual import Individual

sample_path = Path("SAMPLE_TWOLINES/200331-160718_matlab_background=0").resolve()
data_path = Path("fitness_estadistica/cov/0").resolve()
output_path = Path("prueba_blind_deconvolution").resolve()
sigma_start_factor = 0.75

n_sigmas = 1000
sigma_factor = np.linspace(0.5, 1.5, n_sigmas)

n_iter = 100
delta = 1


# Cargo muestra y PSF
with open(sample_path / "report.txt", "r+") as file:
    report_dict = yaml.load(file, Loader=yaml.SafeLoader)

psf_dict = report_dict["PSF"]
psf_dict.update({"sigma": np.sqrt(psf_dict["sigma2"])})

true_psf = GaussianPSF(amplitude=psf_dict["amplitude"], sigma=psf_dict["sigma"], offset=psf_dict["offset"])
sample = Sample.load(sample_path / "sample.npz", subtract_mean=False)


# Cargo datos
data = np.load(data_path / "individual.npz")
individual = Individual(positions=data["positions"][-1], alpha=data["alpha"][-1])


# Inicializo GPU
gpu = GPU(0)
gpu.set_output_shape(sample.roi_shape)


# Selecciono individuo
fitness = np.zeros(n_sigmas)

for i, factor in enumerate(sigma_factor):
    new_sigma = true_psf.sigma * factor
    new_amplitude = 1/np.sqrt(2*np.pi*new_sigma**2)
    new_offset = true_psf.offset
    aux_psf = GaussianPSF(amplitude=new_amplitude, sigma=new_sigma, offset=new_offset)
    convolved = gpu.convolve(individual, aux_psf)
    fitness[i] = correlation_fitness(None, convolved, sample.roi_data)

np.savez(output_path / "fitness_vs_sigma.npz",
         **{"path": str(data_path / "individual.npz"),
            "roi_data": sample.roi_data,
            "true_sigma": true_psf.sigma,
            "sigma_factor": sigma_factor,
            "sigma": true_psf.sigma * sigma_factor,
            "fitness": fitness})


# Gradiente descendiente
fitness_history = []
dfitness_history = []
sigma_history = [true_psf.sigma * sigma_start_factor]

new_amplitude = 1 / np.sqrt(2 * np.pi * sigma_history[-1] ** 2)
new_offset = true_psf.offset
new_psf = GaussianPSF(amplitude=new_amplitude, sigma=sigma_history[-1], offset=new_offset)
convolved = gpu.convolve(individual, new_psf)
fitness_history.append(correlation_fitness(None, convolved, sample.roi_data))

for i in range(n_iter):
    test_sigma = sigma_history[-1] * 1.001
    new_amplitude = 1 / np.sqrt(2 * np.pi * test_sigma ** 2)
    new_offset = true_psf.offset
    test_psf = GaussianPSF(amplitude=new_amplitude, sigma=test_sigma, offset=new_offset)
    convolved = gpu.convolve(individual, test_psf)
    test_fitness = correlation_fitness(None, convolved, sample.roi_data)
    dfitness_history.append((test_fitness - fitness_history[-1]) / (test_sigma - sigma_history[-1]))

    delta_sigma = - dfitness_history[-1] * delta
    print(f"sigma={sigma_history[-1]:0.4f}, test_sigma={test_sigma:0.4f}, "
          f"fitness={fitness_history[-1]:0.6e}, test_fitness={test_fitness:0.6e}, "
          f"dfitness={dfitness_history[-1]:0.3f}, delta_sigma={delta_sigma:0.3f}")

    sigma_history.append(sigma_history[-1] + delta_sigma)
    new_amplitude = 1 / np.sqrt(2 * np.pi * sigma_history[-1] ** 2)
    new_offset = true_psf.offset
    new_psf = GaussianPSF(amplitude=new_amplitude, sigma=sigma_history[-1], offset=new_offset)
    convolved = gpu.convolve(individual, new_psf)
    fitness_history.append(correlation_fitness(None, convolved, sample.roi_data))

sigma_history = np.array(sigma_history)
fitness_history = np.array(fitness_history)


# Figura iteraciones
fig, ax = plt.subplots()
ax.plot(sigma_factor, fitness, linewidth=1, color="k", label="Fitness")
ax.plot(sigma_history[0]/true_psf.sigma, fitness_history[0],
        marker="o", markersize=5, color="red", linewidth=0, label="Inicio", zorder=10)
ax.plot(sigma_history/true_psf.sigma, fitness_history, marker="o", markersize=3, ls=":", linewidth=1,
        label="Gradiente descendiente")
plt.xlabel("Sigma")
plt.ylabel("Fitness")
plt.yscale("log")
# plt.xscale("log")
plt.grid(ls=":")
plt.legend()
plt.tight_layout()
plt.savefig(output_path / "gradiente_descendiente.png")


# Figura error
fig, ax = plt.subplots()
ax.plot(np.abs((sigma_history / true_psf.sigma) - 1), marker="o", markersize=3, linewidth=1, ls=":", color="k")
plt.title("Error relativo")
plt.xlabel("Iteración de gradiente descendiente")
plt.ylabel("sigma/true_sigma - 1")
plt.yscale("log")
plt.grid(ls=":")
plt.savefig(output_path / "error.png")
