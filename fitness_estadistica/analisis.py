#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from pathlib import Path


paths = {"chi": Path("fitness_estadistica/chi").resolve(),
         "cov": Path("fitness_estadistica/cov").resolve()}

savepath = Path("fitness_estadistica").resolve()

data = {}

chicolor = "#b1d900"
covcolor = "#fa6498"

for fitness_name in ["chi", "cov"]:
    run = Path(paths[fitness_name]).resolve()
    iters = np.array([Path(x).resolve() for x in run.iterdir() if x.is_dir()])
    iters = iters[np.argsort([float(x.name) for x in iters])]  # Ordeno por numero de iteracion
    n_iters = iters.size

    print(f"Cargando datos para '{fitness_name}': {n_iters} archivos en {run}")

    # Preparo los datos
    data[fitness_name] = {"best_fitness": [],
                          "std_fitness": [],
                          "relative_fitness": [],
                          "relative_dispersion": [],
                          "line_bias": [],
                          "line_std": [],
                          "normalized_bias": [],
                          "normalized_std": []}
    generation = []

    # Cargo los datos
    for i, path in enumerate(iters):
        this_data = np.load(path / "fitness.npz")
        generation = this_data["generation"]

        for key in data[fitness_name].keys():
            data[fitness_name][key].append(this_data[key])

            # Si es la última iteración, convierto a array de numpy
            if i == n_iters - 1:
                data[fitness_name][key] = np.array(data[fitness_name][key])

    for key in ["line_bias", "line_std", "normalized_bias", "normalized_std"]:
        data[fitness_name]["mean_" + key] = np.mean(data[fitness_name][key], axis=2)

    full_keys = [key for key in this_data.keys()]
    print(f"keys disponibles: {full_keys}\n")
    print("keys guardados:")
    for key in data[fitness_name].keys():
        print(f"    {key}: {data[fitness_name][key].shape}")

data["chi"].update(generation=np.array(generation))
data["cov"].update(generation=np.array(generation))

np.savez("fitness_estadistica/chi_data.npz", **data["chi"])
np.savez("fitness_estadistica/cov_data.npz", **data["cov"])



# Figura de checkeo estadística
for key in ["relative_fitness", "mean_line_std", "mean_line_bias"]:
    x = np.arange(1, n_iters + 1)
    chi_mean = []
    chi_std = []
    chi_min = []
    chi_max = []
    cov_mean = []
    cov_std = []
    cov_min = []
    cov_max = []

    for end in x:
        chi_mean.append(np.mean(data["chi"][key][0:end, -1]))
        chi_std.append(np.std(data["chi"][key][0:end, -1]))
        chi_min.append(np.min(data["chi"][key][0:end, -1]))
        chi_max.append(np.max(data["chi"][key][0:end, -1]))
        cov_mean.append(np.mean(data["cov"][key][0:end, -1]))
        cov_std.append(np.std(data["cov"][key][0:end, -1]))
        cov_min.append(np.min(data["cov"][key][0:end, -1]))
        cov_max.append(np.max(data["cov"][key][0:end, -1]))

    chi_mean = np.array(chi_mean)
    chi_std = np.array(chi_std)
    chi_min = np.array(chi_min)
    chi_max = np.array(chi_max)
    cov_mean = np.array(cov_mean)
    cov_std = np.array(cov_std)
    cov_min = np.array(cov_min)
    cov_max = np.array(cov_max)

    fig, ax = plt.subplots()
    ax.set_title(f"Evolución estadística de {key}")
    ax.plot(x, chi_mean, color=chicolor, label="chi")
    ax.plot(x, chi_min, color=chicolor, ls="--", label="min/max")
    ax.plot(x, chi_max, color=chicolor, ls="--")
    ax.fill_between(x, chi_mean-chi_std, chi_mean+chi_std, color=chicolor, linewidth=0, alpha=0.2, label="+-std",
                    zorder=-10)
    ax.plot(x, cov_mean, color=covcolor, label="cov")
    ax.plot(x, cov_min, color=covcolor, ls="--")
    ax.plot(x, cov_max, color=covcolor, ls="--")
    ax.fill_between(x, cov_mean-cov_std, cov_mean+cov_std, color=covcolor, linewidth=0, alpha=0.2, zorder=-20)
    ax.set_xlabel("Iteraciones")
    plt.legend()
    fig.savefig(savepath / f"estadistica_{key}.png")

    fig, ax = plt.subplots()
    plt.hist(data["chi"][key][:, -1], bins="fd")
    plt.title(f"Chi: {key}")
    fig.savefig(savepath / f"histograma_chi_{key}.png")

    fig, ax = plt.subplots()
    plt.hist(data["cov"][key][:, -1], bins="fd")
    plt.title(f"Cov: {key}")
    fig.savefig(savepath / f"histograma_cov_{key}.png")


# Figura de best_fitness
x = generation
chi_best_fitness_mean = np.mean(data["chi"]["best_fitness"], axis=0)
chi_best_fitness_std = np.std(data["chi"]["best_fitness"], axis=0)
chi_best_fitness_max = np.max(data["chi"]["best_fitness"], axis=0)
chi_best_fitness_min = np.min(data["chi"]["best_fitness"], axis=0)

cov_best_fitness_mean = np.mean(data["cov"]["best_fitness"], axis=0)
cov_best_fitness_std = np.std(data["cov"]["best_fitness"], axis=0)
cov_best_fitness_max = np.max(data["cov"]["best_fitness"], axis=0)
cov_best_fitness_min = np.min(data["cov"]["best_fitness"], axis=0)

plt.figure()
ax = plt.subplot()
ax.set_facecolor("none")
ax.plot(generation, chi_best_fitness_mean, linewidth=1, color=chicolor, label="chi")
ax.plot(generation, np.array([chi_best_fitness_min, chi_best_fitness_max]).T, ls="--", linewidth=1, color=chicolor)
ax.fill_between(generation, chi_best_fitness_mean - chi_best_fitness_std, chi_best_fitness_mean + chi_best_fitness_std,
                linewidth=0, facecolor=chicolor, zorder=-30, alpha=0.2)
ax.set_ylabel("Chi")


axb = ax.twinx()
axb.set_zorder(-1)
axb.plot(generation, cov_best_fitness_mean, linewidth=1, color=covcolor, label="cov")
axb.plot(generation, np.array([cov_best_fitness_min, cov_best_fitness_max]).T, ls="--", linewidth=1, color=covcolor)
axb.fill_between(generation, cov_best_fitness_mean - cov_best_fitness_std, cov_best_fitness_mean + cov_best_fitness_std,
                 linewidth=0, facecolor=covcolor, zorder=-20, alpha=0.2)
axb.set_ylabel("Correlación")

plt.xscale("log")
ax.set_yscale("log")
axb.set_yscale("log")
plt.legend()
plt.title("Mejor fitness")
plt.savefig(savepath / f"best_fitness.png")
