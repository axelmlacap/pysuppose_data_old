#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from pathlib import Path
import time
import yaml

from pysuppose import SupposeEngine
from pysuppose.engine import fitness_function, correlation_fitness, after_convolve_recalculate_alpha, \
    after_convolve_subtract_mean, evolve_function
import pysuppose.ga_operators.matlab_ga_operators as matlab_ga_operators
import pysuppose.ga_operators.blind_ga_operators as blind_ga_operators
from pysuppose.bases.psf import GaussianPSF, FunctionPSF
from pysuppose.bases.device import CPU, GPU
from pysuppose.bases.population import Population
from pysuppose.bases.sample import Sample
from pysuppose.bases.individual import Individual
from pysuppose.sample_generators.twolines import PimpedTwoLines
from pysuppose.addons.metrics import FitnessMetrics, DiversityMetrics, TwoLinesMetrics, SelectionMonitor
from pysuppose.addons.plots import SimplePlot, AttributePlot, ConvolvedMonitor
from pysuppose.addons.report import ReportAddOn
from pysuppose.addons.save import SaveArray, SaveHDF5, SavePopulation
from pysuppose.bases.individual_generator import SimpleIndividualGenerator
from pysuppose.addons.blind_deconvolution import GradientDescent, FitnessMapPlot

import logging

TEST = True

TIMESTAMP = time.strftime('%y%m%d.%H%M%S')
ROOT: Path = Path(__file__).resolve().parents[1].resolve()
PROJECT_PATH: Path = Path(__file__).resolve().parent.resolve()
PROJECT_NAME = PROJECT_PATH.name
OUTPUT_PATH = PROJECT_PATH / ("output" if not TEST else "test")
OUTPUT_PATH.mkdir(exist_ok=True)


# Parámetros
# Los sigma de los individuos iniciales se van a tomar como una gaussiana centrada en init_sigma * true_sigma y con
# desviación estándar init_sigma_std * init_sigma * true_sigma
init_sigma = 0.95  # En unidades de true_sigma
init_sigma_std = 0.03  # En unidades de init_sigma * true_sigma
blind_mutation_radius = 1e-3
blind_parameters = ["sigma"]
do_blind_deconvolution = True

# Configuración de la ejecución
subtract_mean = False
n_iterations = 10000
gpu_number = 0
addon_interval = 100
addon_interval_mode = "logspace"
plot_interval = 100
plot_interval_mode = "logspace"

# Población
n_sources = 426
population_size = 500
elite_size = 1
kick_radius = 0.5  # En unidades de init_sigma

# Muestra
sample_folder = ROOT / "SAMPLE_TWOLINES/200331-160718_matlab_background=0"
sample_name = "sample.npz"
sample_path = sample_folder / sample_name

# Algoritmo
fitness = correlation_fitness
evolution = evolve_function
selection = matlab_ga_operators.select
cross = blind_ga_operators.mixed_cross
mutation = blind_ga_operators.gaussian_mutation

# Nombre de la corrida
run_name = f"mean{init_sigma:.2f}_std={init_sigma_std:.2f}_radius={blind_mutation_radius:.1e}" \
           f"{'_(control)' if not do_blind_deconvolution else ''}"
run_path = OUTPUT_PATH / run_name
run_path.mkdir(exist_ok=False, parents=True)


# Carga muestra, PSF e individuos iniciales
with open(sample_folder / "report.txt", "r+") as file:
    report_dict = yaml.load(file, Loader=yaml.SafeLoader)

psf_dict = report_dict["PSF"]
psf_dict.update({"sigma": np.sqrt(psf_dict["sigma2"])})
sample_generator_dict = PimpedTwoLines.load(sample_folder / "sample_generator.npz").to_dict()
population_generator_dict = report_dict["SimpleIndividualGenerator"]


# Cargo PSF
true_psf = GaussianPSF(amplitude=psf_dict["amplitude"], sigma=psf_dict["sigma"], offset=psf_dict["offset"])

sigma_start = init_sigma * true_psf.sigma
amplitude_start = 1/np.sqrt(2*np.pi*sigma_start**2)
offset_start = true_psf.offset
psf = GaussianPSF(amplitude=amplitude_start, sigma=sigma_start, offset=offset_start)


# Cargo muestra
sample = Sample.load(sample_path, subtract_mean=subtract_mean)
sample.save(run_path / "sample.npz")


# Cargo población y redefino elite
psf_integral = psf.integrate(sample.roi_shape)
alpha = np.sum(sample.roi_data) / (n_sources * psf_integral) - 0.1/n_sources

generator = SimpleIndividualGenerator(sample=sample,
                                      psf=psf,
                                      alpha=alpha,
                                      subtract_mean=subtract_mean,
                                      iterations=5000,
                                      mode="from_image",
                                      window_size=1)

population = generator.generate_population(size=population_size,
                                           elite=elite_size,
                                           kick_radius=kick_radius * psf.sigma,
                                           template="kick_first")

# Les agrego un sigma
for ind in population.new_generation:
    if do_blind_deconvolution:
        ind.sigma = np.random.normal(loc=psf.sigma, scale=true_psf.sigma * init_sigma_std)
    else:
        ind.sigma = psf.sigma

generator.plot_result(path=run_path / "initial_population.png")
plt.close()
population.export_generation_hdf5(path=run_path / "initial_population.hdf5")


# Inicializo el engine que voy a usar
engine = SupposeEngine(sample=sample, psf=psf, population=population)
engine.resource_manager.add_device(GPU(gpu_number))

if subtract_mean:
    engine.register_as_checkpoint(after_convolve_subtract_mean)  # Resta valor medio

# Autoguardado
engine.auto_save_path = run_path / 'autosave_population.hdf5'
engine.auto_save_time = 300


# Configuro parámetros del algoritmo genético
# Fitness
engine.register_as_fitness(fitness)
# Evolución
engine.register_as_evolve(evolution)
# Selección
engine.register_as_selection(selection)
# Cruza
engine.register_as_cross(cross)
engine.crossover.cross_percent = 0.4  # Porcentaje de cruzas realizadas
if do_blind_deconvolution:
    engine.crossover.parameters = blind_parameters
# Mutación
engine.register_as_mutation(mutation)
engine.mutation.mutate_percent = 0.2
engine.mutation.mutate_radius = 0.01 * psf.sigma
if do_blind_deconvolution:
    engine.mutation.parameters = blind_parameters
    engine.mutation.parameter_radiuses = [blind_mutation_radius * psf.sigma]


# Inicializo addons
# Reporte
report = ReportAddOn(interval_value=addon_interval,
                     interval_mode=addon_interval_mode)
report.setup_console()
report.setup_file(run_path / f'report.txt')

# Configuración de addons
cmap = cm.get_cmap("tab10")

# Blind deconvolution
fitness_map_plot = FitnessMapPlot(x="sigma", relative_limits=(0.75, 1.25), size=100, true_value=true_psf.sigma,
                                  interval_mode=addon_interval_mode,
                                  interval_value=addon_interval,
                                  path=run_path / "fitness_map.png",
                                  file_per_interval=True)

# Mejor individuo
simple_plot = SimplePlot(interval_value=addon_interval,
                         interval_mode=addon_interval_mode,
                         path=run_path / 'mejor_individuo.png',
                         file_per_interval=True)

save_best_individual = SaveArray(attributes={'positions': 'engine.population.best_individual.positions',
                                             'alpha': 'engine.population.best_individual.alpha',
                                             'fitness': 'engine.population.best_individual.fitness'},
                                 path=run_path / 'individual.npz',
                                 file_per_interval=False,
                                 interval_value=addon_interval,
                                 interval_mode=addon_interval_mode)

# Población
save_population = SavePopulation(path=run_path / "population.hdf5",
                                 file_per_interval=True,
                                 interval_value=addon_interval,
                                 interval_mode=addon_interval_mode)

# Métricas de fitness
fitness_metrics = FitnessMetrics(noise_pattern=sample_generator_dict["noise_pattern"],
                                 slope_window=100,
                                 additional_fitness=[fitness_function, correlation_fitness],
                                 store=True,
                                 interval_value=addon_interval,
                                 interval_mode=addon_interval_mode)

plot_absolute_fitness = AttributePlot({"Mejor fitness": "engine.population.best_fitness",
                                       "Promedio": "engine.population.mean_fitness",
                                       "Std": "engine.population.std_fitness"},
                                      axes_directives=[{"xscale": "log", "yscale": "log", "linecolor": cmap(0)},
                                                       {"xscale": "log", "yscale": "log", "linecolor": cmap(1)},
                                                       {"xscale": "log", "yscale": "log", "linecolor": cmap(2)}],
                                      path=run_path / 'fitness_absolute.png',
                                      file_per_interval=False,
                                      interval_value=plot_interval,
                                      interval_mode=plot_interval_mode)

plot_relative_fitness = AttributePlot({"Mejor / ideal": "engine.population.relative_fitness",
                                       "Std / Promedio": "engine.population.relative_dispersion"},
                                      axes_directives={"xscale": "log", "yscale": "log"},
                                      path=run_path / 'fitness_relative.png',
                                      file_per_interval=False,
                                      interval_value=plot_interval,
                                      interval_mode=plot_interval_mode)

# Métricas de dos rectas
twolines_metrics = TwoLinesMetrics(true_centers=sample_generator_dict["lines_centre"],
                                   angle=sample_generator_dict["angle"],
                                   true_sigma=psf_dict["sigma"],
                                   sample=sample_generator_dict["image"],
                                   true_sample=sample_generator_dict["true_image"],
                                   im_shape=sample.roi_shape,
                                   plot=True,
                                   path=run_path / "likelyhood_fit.png",
                                   file_per_interval=True,
                                   interval_value=addon_interval,
                                   interval_mode=addon_interval_mode)

plot_twolines_metrics = AttributePlot({"Normalized bias": "engine.population.mean_normalized_bias",
                                       "Improvement": "engine.population.mean_improvement"},
                                      path=run_path / 'twolines_attr.png',
                                      axes_directives=[{"xscale": "log", "yscale": "log", "color": cmap(3)},
                                                       {"xscale": "log", "yscale": "linear", "color": cmap(4)}],
                                      file_per_interval=False,
                                      interval_value=plot_interval,
                                      interval_mode=plot_interval_mode)

plot_auxiliar = AttributePlot({"Best sigma": "engine.population.best_sigma",
                               "Sigma mean": "engine.population.sigma_mean",
                               "Sigma std / mean": "engine.population.sigma_relative_dispersion"},
                              path=run_path / 'sigma.png',
                              axes_directives={"xscale": "log", "yscale": "linear"},
                              file_per_interval=False,
                              interval_value=plot_interval,
                              interval_mode=plot_interval_mode)

save_data = SaveArray(attributes={
    # Fitness metrics
    "best_fitness": "engine.population.best_fitness",
    "worst_fitness": "engine.population.worst_fitness",
    "mean_fitness": "engine.population.mean_fitness",
    "variability": "engine.population.variability",
    "ptp_dispersion": "engine.population.ptp_dispersion",
    "std_fitness": "engine.population.std_fitness",
    "ideal_fitness": "engine.population.ideal_fitness",
    "relative_fitness": "engine.population.relative_fitness",
    "relative_dispersion": "engine.population.relative_dispersion",
    "fitness_slope": "engine.population.fitness_slope",
    "fitness_logslope": "engine.population.fitness_logslope",
    "fitness": "engine.population.fitness",
    # TwoLinesMetrics
    'line_centre': 'engine.population.centre',
    'line_bias': 'engine.population.bias',
    'line_std': 'engine.population.std',
    'normalized_bias': 'engine.population.normalized_bias',
    'normalized_std': 'engine.population.normalized_std',
    # Auxiliar
    'best_sigma': 'engine.population.best_individual.sigma',
    'sigma_mean': 'engine.population.sigma_mean',
    'sigma_std': 'engine.population.sigma_std'
},
    path=run_path / "output.npz",
    file_per_interval=False,
    interval_value=addon_interval,
    interval_mode=addon_interval_mode)

engine.connect(report, simple_plot)
engine.connect(fitness_metrics, plot_relative_fitness, plot_absolute_fitness)
engine.connect(twolines_metrics, plot_twolines_metrics)
engine.connect(plot_auxiliar, fitness_map_plot)
engine.connect(save_best_individual, save_population, save_data)

# Corro el algoritmo
engine.run(n_iterations)
