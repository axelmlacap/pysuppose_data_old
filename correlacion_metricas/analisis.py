import numpy as np
from pathlib import Path
from typing import Union, List
import matplotlib.pyplot as plt
import matplotlib.cm as cm

cmap = cm.get_cmap("tab10")

for i in range(20):
    plt.plot(np.arange(10), np.ones(10)*i, color=cmap(i), linewidth=10)
plt.show()

# Lista de datos:
#
# mean_individual
# center_of_mass
# population_spd
# individual_spd
# best_spd
# generation
# best_fitness
# worst_fitness
# mean_fitness
# variability
# ptp_dispersion
# std_fitness
# ideal_fitness
# relative_fitness
# relative_dispersion
# fitness_slope
# fitness_logslope
# fitness
# line_centre
# line_bias
# line_std


def load_data(path: Union[str, Path], extension: str = ".npz"):
    path = Path(path).resolve()

    file_names = ["diversity",
                   "fitness",
                   "twolines_metrics"]

    data = {}

    for fname in file_names:
        fpath = (path / fname).with_suffix(extension)
        if fpath.exists():
            fdata = np.load(fpath)
            data.update(fdata)

    return data


path = "/home/axel/pysuppose_data/Axel/seleccion/200214-145558_tournament-tournament_size=10"

data = load_data(path)
data["line_bias"] = np.abs(data["line_bias"]).mean(axis=1)
data["line_std"] = np.abs(data["line_std"]).mean(axis=1)

# plt.scatter(data["relative_dispersion"], data["population_spd"], s=3)
# plt.show()

# Fig 1
vars = ["population_spd",
        "best_spd",
        "best_fitness",
        "mean_fitness",
        "std_fitness",
        "relative_fitness",
        "relative_dispersion",
        "fitness_slope",
        "fitness_logslope",
        "line_bias",
        "line_std"]

size = 4 # [in]
n = len(vars)
fig, ax = plt.subplots(n, n)
fig.set_size_inches(ax.shape[1]*size, ax.shape[0]*size)

for i in range(n):
    for j in range(n):
        # Fig 1
        print(vars[i], vars[j])
        if i != j: ax[i, j].scatter(data[vars[j]], data[vars[i]], 2, color="#000000")
        # if j == 0: ax[i, j].set_ylabel(vars[i])
        ax[i, j].set_ylabel(vars[i])
        # if i == n-1: ax[i, j].set_xlabel(vars[j])
        ax[i, j].set_xlabel(vars[j])
        ax[i, j].get_xaxis().set_ticklabels([])
        ax[i, j].get_yaxis().set_ticklabels([])

plt.tight_layout()
fig.savefig("scatters.pdf")

plt.show()
