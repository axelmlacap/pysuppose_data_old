import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt

base_folder = Path("diversidad/").resolve()
folders = [base_folder / "200213-151219_standard-mutate_radius=0.01*sigma_psf",
           base_folder / "200213-182906_standard-mutate_radius=0.1*sigma_psf",
           base_folder / "200214-100025_standard-mutate_radius=1*sigma_psf"]

diversity = []
fitness = []
for folder in folders:
    diversity.append(np.load(folder / "diversity.npz"))
    fitness.append(np.load(folder / "fitness.npz"))

fig, ax = plt.subplots()
for data in diversity:
    ax.plot(data["generation"], data["population_spd"])
ax.set_xscale("log")
ax.set_yscale("log")
ax.set_xlabel("Generación")
ax.set_ylabel("SPD")

fig, ax = plt.subplots()
for data in diversity:
    ax.plot(data["generation"], data["best_spd"])
ax.set_xscale("log")
ax.set_yscale("log")
ax.set_xlabel("Generación")
ax.set_ylabel("Best individual SPD")

fig, ax = plt.subplots()
for i, (data1, data2) in enumerate(zip(diversity, fitness)):
    ax.plot(data2["best_fitness"], data1["population_spd"])
ax.set_xlabel("Fitness")
ax.set_ylabel("SPD")

fig, ax = plt.subplots()
for i, (data1, data2) in enumerate(zip(diversity, fitness)):
    ax.plot(data2["best_fitness"], data1["best_spd"])
ax.set_xlabel("Fitness")
ax.set_ylabel("Best individual SPD")

fig, ax = plt.subplots()
for i, (data1, data2) in enumerate(zip(diversity, fitness)):
    ax.plot(data2["std_fitness"], data1["population_spd"])
ax.set_xlabel("Std fitness")
ax.set_ylabel("SPD")

fig, ax = plt.subplots()
for i, (data1, data2) in enumerate(zip(diversity, fitness)):
    ax.plot(data2["std_fitness"], data1["best_spd"])
ax.set_xlabel("Std fitness")
ax.set_ylabel("Best individual SPD")

plt.show()
